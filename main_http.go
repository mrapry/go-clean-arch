package main

import (
	"fmt"
	"github.com/labstack/echo"
	mid "github.com/labstack/echo/middleware"
)

// EchoServer structure
type EchoServer struct {
}

// time now struct
type timenow struct {
	Datetime string `json:"datetime"`
}

// Run main function for serving echo http server
func (s *EchoServer) Run() {
	fmt.Println("Echo Server is run ...")
}

// Run main function for serving echo http server
func (s *EchoServer) Run() {
	e := echo.New()
	e.use(mid.CORS())

	if env.GetEnv().Development == "1" {
		e.Debug = true
	}

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Service up and running !!")
	})

	e.GET("/api/timenow", func(c echo.Context) error {
		return c.JSON(http.StatusOK, &timenow{
			Datetime: time.Now().Format(time.RFC3339),
		})
	})

	// set listener port
	listenerPort := fmt.Sprintf(":%d", env.GetEnv().HTTPPort)
	e.Logger.Fatal(e.Start(listenerPort))
}

// Constructor Main Http
func MainHttp() (*EchoServer, error) {

	// Initiate DB Write And Read
	gorm.DBWriteInit()
	gorm.DBReadInit()


	return &EchoServer{}, nil
}
