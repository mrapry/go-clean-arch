package gorm_postgree

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func WriteDBPostgree() *gorm.DB {
	return CreateDBConnection(fmt.Sprintf("host=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		os.Getenv("WRITE_DB_HOST"), os.Getenv("WRITE_DB_USER"), os.Getenv("WRITE_DB_PASSWORD"), os.Getenv("WRITE_DB_NAME")))

}

// ReadPostgresDB function for creating database connection for write-access
func ReadPostgresDB() *gorm.DB {
	return CreateDBConnection(fmt.Sprintf("host=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		os.Getenv("READ_DB_HOST"), os.Getenv("READ_DB_USER"), os.Getenv("READ_DB_PASSWORD"), os.Getenv("READ_DB_NAME")))
}

// CreateDBConnection function for creating database connection
func CreateDBConnection(descriptor string) *gorm.DB {
	db, err := gorm.Open("postgres", descriptor)
	if err != nil {
		defer db.Close()
		return db
	}

	return db
}

// CloseDb function for closing database connection
func CloseDb(db *gorm.DB) {
	if db != nil {
		db.Close()
		db = nil
	}
}
