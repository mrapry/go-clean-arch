package main

import (
	"errors"
	"fmt"
	"os"
	"sync"

	"go-clean-arch/config/env"
	"github.com/joho/godotenv"
)

func main() {
	var err error
	err = godotenv.Load(".env")
	if err != nil {
		err := errors.New(".env is not loaded properly")
		fmt.Printf(err.Error())
		os.Exit(1)
	}

	env.InitEnvironment() // Load env config to in memory

	echoServer, err := MainHttp()

	if err != nil {
		fmt.Println("Echo Server initial, ", err)
		os.Exit(2)
	}

	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()
		echoServer.Run()
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		ServiceKafka()
	}()

	wg.Wait()
}
